<?php

namespace App\Model;

use PDO;
use PDOException;

//print_r(PDO::getAvailableDrivers());

class Database
{
    public $DBH;
    public $host="localhost";
    public $dbname= "atomic_project_b35";
    public $user="root";
    public $pass="";

    public function __construct()
    {
        try
        {

# MySQL with PDO_MYSQL
            $DBH = new PDO("mysql:host=$this->host;dbname=$this->dbname", $this->user, $this->pass);
           echo"Connected Successfully";
        }
        catch(PDOException $e){
            echo $e->getMessage();
        }

    }
}
//$objDatabase=new Database();